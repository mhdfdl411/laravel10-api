<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;
    /**
     * fillable
     *
     * @var array
     */
    protected $fillable = [
        'book_id',
        'title',
        'author',
        'publisher',
        'publish_date',
        'book_thickness',
        'status',
        'borrower',
        'borrow_date',
        'return_date'
    ];
}
