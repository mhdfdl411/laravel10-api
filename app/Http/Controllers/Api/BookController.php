<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\BookResource;
use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{
    /**
     * index
     *
     * @return BookResource
     */
    public function index()
    {
        //get all books
        $books = Book::all();

        //get using paginate
//        $books = Book::latest()->paginate(5);

        //return collection of books as a resource
        return new BookResource(true, 'List Data Buku', $books);
    }

    public function filterbook($status) {
        //find book by ID
        $book = Book::where('status', $status)->get(['book_id','title','author', 'publisher', 'publish_date', 'book_thickness', 'status', 'borrower', 'borrow_date', 'return_date']);



        //return single book as a resource
        return new BookResource(true, 'List Filtered Data Buku', $book);
    }

    /**
     * store
     *
     * @param  mixed $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'book_id'     => 'required',
            'title'     => 'required',
            'author'   => 'required',
            'publisher'   => 'required',
            'publish_date'   => 'required',
            'book_thickness'   => 'required',
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //create book
        $book = Book::create([
            'book_id'     => $request->book_id,
            'title'     => $request->title,
            'author'   => $request->author,
            'publisher'   => $request->publisher,
            'publish_date'   => $request->publish_date,
            'book_thickness'   => $request->book_thickness,
        ]);

        //return response
        return new BookResource(true, 'Buku Berhasil Ditambahkan!', $book->get());
    }

    /**
     * show
     *
     * @param  mixed $book
     * @return BookResource
     */
    public function show($book_id)
    {
        //find book by book_id
        $book = Book::where('book_id',$book_id);

        //return single book as a resource
        return new BookResource(true, 'Detail Data Buku!', $book->get());
    }

    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $book
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $book_id)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'book_id'     => 'required',
            'title'     => 'required',
            'author'   => 'required',
            'publisher'   => 'required',
            'publish_date'   => 'required',
            'book_thickness'   => 'required',
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //find book by book_id
        $book = Book::where('book_id',$book_id);


        $book->update([
            'book_id'     => $request->book_id,
            'title'     => $request->title,
            'author'   => $request->author,
            'publisher'   => $request->publisher,
            'publish_date'   => $request->publish_date,
            'book_thickness'   => $request->book_thickness,
            ]);

        //return response
        return new BookResource(true, 'Data Buku Berhasil Diubah!', $book->get());
    }

    public function updateborrowstatus(Request $request, $book_id)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'status'     => 'required'
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //find book by book_id
        $book = Book::where('book_id',$book_id);


        $book->update([
            'status'     => $request->status,
            'borrower'     => $request->borrower,
            'borrow_date'   => $request->borrow_date,
            'return_date'   => $request->return_date,
        ]);

        //return response
        return new BookResource(true, 'Data Buku Berhasil Diubah!', $book->get());
    }

    /**
     * destroy
     *
     * @param  mixed $book
     * @return BookResource
     */
    public function destroy($book_id)
    {

        //find book by book_id
        $book = Book::where('book_id',$book_id);

        //delete book
        $book->delete();

        //return response
        return new BookResource(true, 'Data Buku Berhasil Dihapus!', null);
    }

}
