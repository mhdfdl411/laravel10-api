<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//API route for register new user
Route::post('/register', [App\Http\Controllers\API\AuthController::class, 'register']);
//API route for login user
Route::post('/login', [App\Http\Controllers\API\AuthController::class, 'login']);

//Protecting Routes
Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/profile', function(Request $request) {
        return auth()->user();
    });

    // API route for logout user
    Route::post('/logout', [App\Http\Controllers\API\AuthController::class, 'logout']);
});

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

//posts
Route::apiResource('/posts', App\Http\Controllers\Api\PostController::class);

//book
Route::apiResource('/books', App\Http\Controllers\Api\BookController::class);

Route::get('/books/filterbook/{status}', [App\Http\Controllers\API\BookController::class, 'filterbook']);
Route::put('/books/updateborrowstatus/{book_id}', [App\Http\Controllers\API\BookController::class, 'updateborrowstatus']);


